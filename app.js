'use strict';



/*


    HOW TO IMPLEMENT SOUNDCLOUD EMBEDS:


        - remove all layout attributes found before `src` and replace them with `class="test-0 music-detail-footer-embed-ep"` or `class="test-0 music-detail-footer-embed-single"`

        - replace the value of `&color=%########` with `&color=%23446678` in <iframe>

        - remove `&visual=true` at the end of `<iframe>`

        - add `\\\` before every `"` (except the last one of course)

        - beware of `newlines`

        - use this link for help on validating JSONs: `https://jsonlint.com/`

*/



const jsonReleases = `[
    {
        "id": 0,
        "title": "Rumbo Marimbula",
        "artist": "Novad",
        "ref": "CR01",
        "date": "2022-04-12",
        "cover": "rumbo_marimbula.jpg",
        "bio": [
            "Rumbo Marimbula es el primer EP de nuestro sello Chiriquí Records. Inspirado por los grandes artistas de la folktronica Latina, el ep combina instrumentos como el balafón, la marimba o el charango. Una canción, mas pop, llamada Camino Hacia el Oeste, fué grabada en Panamá con la voz de nuestro artista local, Novad.",
            "La primera canción, Rumbo Marimbula, adopta un estilo chill con ritmos y instrumentos africanos. Varios elementos de la canción, como las transiciones por ejemplo son orgánicos, con sonidos acuáticos por ejemplo. La segunda canción, Vamos Palante, es mas bailable y se inspira de la música del Pacífico Colombiano, con la presencia de una marimba.",
            "La segunda parte de la canción adoptó un estilo libre, con ritmos de madera y melodías de Charango. La tercera canción es totalmente diferente del estilo folktronic del ep y se compone con un acento más pop. La última canción, Bri Bri, es lenta, tribal, étnica, y nos ofrece una vibra downtempo típica de nuestro sello."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-ep\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1423790692&amp;color=%23446678&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true\\\"></iframe><div class=\\\"test-0 detail-footer-embed-bottom\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/sets/cr01-novad-rumbo-marimbula-chiriqui-rec\\\" title=\\\"CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)\\\" target=\\\"_blank\\\">CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)</a></div>"
    },
    {
        "id": 1,
        "title": "Toko Toko",
        "artist": "Novad",
        "ref": null,
        "date": "2022-06-03",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 2,
        "title": "Tz Onot",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "tz_onot.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243319620&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/chiriqui-sessions-01-tropical-set-novad\\\" title=\\\"Chiriquí Sessions 01 : Tropical Set - Novad\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc;text - decoration: none;\\\">Chiriquí Sessions 01 : Tropical Set - Novad</a></div>"
    },
    {
        "id": 3,
        "title": "Panama Pacifico",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 4,
        "title": "Rumbo Marimbula",
        "artist": "Novad",
        "ref": "CR01",
        "date": "2022-04-12",
        "cover": "rumbo_marimbula.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-ep\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1423790692&amp;color=%23446678&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true\\\"></iframe><div class=\\\"test-0 detail-footer-embed-bottom\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/sets/cr01-novad-rumbo-marimbula-chiriqui-rec\\\" title=\\\"CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)\\\" target=\\\"_blank\\\">CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)</a></div>"
    },
    {
        "id": 5,
        "title": "Toko Toko",
        "artist": "Novad",
        "ref": null,
        "date": "2022-06-03",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 6,
        "title": "Tz Onot",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "tz_onot.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243319620&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/chiriqui-sessions-01-tropical-set-novad\\\" title=\\\"Chiriquí Sessions 01 : Tropical Set - Novad\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc;text - decoration: none;\\\">Chiriquí Sessions 01 : Tropical Set - Novad</a></div>"
    },
    {
        "id": 7,
        "title": "Panama Pacifico",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 8,
        "title": "Rumbo Marimbula",
        "artist": "Novad",
        "ref": "CR01",
        "date": "2022-04-12",
        "cover": "rumbo_marimbula.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-ep\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1423790692&amp;color=%23446678&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true\\\"></iframe><div class=\\\"test-0 detail-footer-embed-bottom\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/sets/cr01-novad-rumbo-marimbula-chiriqui-rec\\\" title=\\\"CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)\\\" target=\\\"_blank\\\">CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)</a></div>"
    },
    {
        "id": 9,
        "title": "Toko Toko",
        "artist": "Novad",
        "ref": null,
        "date": "2022-06-03",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 10,
        "title": "Tz Onot",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "tz_onot.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243319620&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/chiriqui-sessions-01-tropical-set-novad\\\" title=\\\"Chiriquí Sessions 01 : Tropical Set - Novad\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc;text - decoration: none;\\\">Chiriquí Sessions 01 : Tropical Set - Novad</a></div>"
    },
    {
        "id": 11,
        "title": "Panama Pacifico",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 12,
        "title": "Rumbo Marimbula",
        "artist": "Novad",
        "ref": "CR01",
        "date": "2022-04-12",
        "cover": "rumbo_marimbula.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-ep\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1423790692&amp;color=%23446678&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true\\\"></iframe><div class=\\\"test-0 detail-footer-embed-bottom\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/sets/cr01-novad-rumbo-marimbula-chiriqui-rec\\\" title=\\\"CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)\\\" target=\\\"_blank\\\">CR01 - Novad - Rumbo Marimbula (Chiriquí Rec)</a></div>"
    },
    {
        "id": 13,
        "title": "Toko Toko",
        "artist": "Novad",
        "ref": null,
        "date": "2022-06-03",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 14,
        "title": "Tz Onot",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "tz_onot.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243319620&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/chiriqui-sessions-01-tropical-set-novad\\\" title=\\\"Chiriquí Sessions 01 : Tropical Set - Novad\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc;text - decoration: none;\\\">Chiriquí Sessions 01 : Tropical Set - Novad</a></div>"
    },
    {
        "id": 15,
        "title": "Panama Pacifico",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 16,
        "title": "Toko Toko",
        "artist": "Novad",
        "ref": null,
        "date": "2022-06-03",
        "cover": "rumbo_marimbula.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    },
    {
        "id": 17,
        "title": "Tz Onot",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "tz_onot.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Spotify",
                "url": "https://www.spotify.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243319620&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/chiriqui-sessions-01-tropical-set-novad\\\" title=\\\"Chiriquí Sessions 01 : Tropical Set - Novad\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc;text - decoration: none;\\\">Chiriquí Sessions 01 : Tropical Set - Novad</a></div>"
    },
    {
        "id": 18,
        "title": "Panama Pacifico",
        "artist": "Novad",
        "ref": null,
        "date": "",
        "cover": "panama_pacifico.jpg",
        "bio": [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ],
        "links": [
            {
                "name": "Bandcamp",
                "url": "https://bandcamp.com"
            },
            {
                "name": "Beatport",
                "url": "https://www.beatport.com"
            },
            {
                "name": "Google Play",
                "url": "https://play.google.com"
            },
            {
                "name": "Youtube",
                "url": "https://www.youtube.com"
            }
        ],
        "embed": "<iframe class=\\\"test-0 music-detail-footer-embed-single\\\" src=\\\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1280418871&color=%23446678&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true\\\"></iframe><div style=\\\"font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;\\\"><a href=\\\"https://soundcloud.com/chiriquirecords\\\" title=\\\"Chiriquí Records\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Chiriquí Records</a> · <a href=\\\"https://soundcloud.com/chiriquirecords/free-dl-novad-toko-toko\\\" title=\\\"Free DL : Novad - Toko Toko\\\" target=\\\"_blank\\\" style=\\\"color: #cccccc; text-decoration: none;\\\">Free DL : Novad - Toko Toko</a></div>"
    }
]`;


const parsedReleases = JSON.parse(jsonReleases);

const header = document.querySelector('.header');
const headerLogoLink = document.querySelector('.header-logo-link');
const musicDetail = document.querySelector('.music-detail');
const musicDetailMainLeftCoverImg = document.querySelector('.music-detail-main-left-cover-img');
const musicDetailMainLeftLinks = document.querySelector('.music-detail-main-left-links');
const musicDetailMainRightTitle = document.querySelector('.music-detail-main-right-title');
const musicDetailMainRightBio = document.querySelector('.music-detail-main-right-bio');
const musicDetailFooterEmbed = document.querySelector('.music-detail-footer-embed');
const musicDetailFooterNavBackLink = document.querySelector('.music-detail-footer-nav-back-link');
const musicDetailFooterNavPrevnext = document.querySelector('.music-detail-footer-nav-prevnext');
const musicGridDisplay = document.querySelector('.music-grid');

let selectedReleaseIndex = 0;


function setReleaseDetailLinks(linksArray) {
    musicDetailMainLeftLinks.innerHTML = '';

    for (let i = 0; i < linksArray.length; i++){
        
        const linkContainer = document.createElement('div');

        const link = document.createElement('a');
        linkContainer.setAttribute('class', 'test-0 music-detail-main-left-link');
        link.target = '_blank';
        link.textContent = linksArray[i].name;
        link.href = linksArray[i].url;
        linkContainer.appendChild(link);

        if(i < linksArray.length - 1){
            const linkSeparator = document.createElement('div');
            linkSeparator.setAttribute('class', 'test-0 music-detail-main-left-link-separator');
            linkSeparator.innerHTML = '&nbsp;/&nbsp;';
            linkContainer.appendChild(linkSeparator);
        }
        
        musicDetailMainLeftLinks.appendChild(linkContainer);
    }
}

function setReleaseDetailBio(bioArray) {
    musicDetailMainRightBio.innerHTML = '';

    for (let i = 0; i < bioArray.length; i++) {
        const paragraph = document.createElement('p');
        paragraph.textContent = bioArray[i];
        musicDetailMainRightBio.appendChild(paragraph);
    }
}

function setReleaseDetailNavPrevNext() {
    musicDetailFooterNavPrevnext.innerHTML = '';

    if(selectedReleaseIndex != 0){
        const prev = document.createElement('a');
        prev.textContent = 'prev';

        prev.addEventListener('click', function(){
            selectedReleaseIndex -= 1;
            setReleaseDetail();
            musicDetail.scrollIntoView();
        });

        musicDetailFooterNavPrevnext.appendChild(prev);
    }

    if(selectedReleaseIndex != 0 && selectedReleaseIndex != parsedReleases.length - 1){
        const separator = document.createElement('div');
        separator.setAttribute('class', 'test-0 test-0 music-detail-footer-nav-prevnext-separator');
        separator.innerHTML = '&nbsp;/&nbsp;';
        musicDetailFooterNavPrevnext.appendChild(separator);
    }

    if(selectedReleaseIndex != parsedReleases.length - 1){
        const next = document.createElement('a');
        next.textContent = 'next';

        next.addEventListener('click', function(){
            selectedReleaseIndex += 1;
            setReleaseDetail();
            musicDetail.scrollIntoView();
        });

        musicDetailFooterNavPrevnext.appendChild(next);
    }

}

function setReleaseDetail() {
    const release = parsedReleases[selectedReleaseIndex]
    musicDetailMainLeftCoverImg.src = 'images/covers/' + release.cover;
    musicDetailMainRightTitle.textContent = release.title;
    setReleaseDetailLinks(release.links);
    setReleaseDetailBio(release.bio);
    musicDetailFooterEmbed.innerHTML = release.embed;
    setReleaseDetailNavPrevNext();
}

function displayGrid(grid){
    for (let i = 0; i < grid.length; i++) {

        const release = document.createElement('div');
        const releaseLink = document.createElement('a');
        const releaseCover = document.createElement('img');
        const releaseTitle = document.createElement('div');

        release.setAttribute('class', 'test-0 music-grid-card');
        release.dataset.releaseIndex = i;
        release.style.setProperty('--cardStaggerAnimationDelay', `${i*50}ms`);

        releaseLink.addEventListener('click', function(){
            selectedReleaseIndex = Number(release.dataset.releaseIndex);
            setReleaseDetail();
            musicDetail.scrollIntoView();
        });

        releaseCover.src = 'images/covers/' + grid[i].cover;

        releaseTitle.setAttribute('class', 'test-0 music-grid-card-title');
        releaseTitle.textContent = grid[i].title;

        releaseLink.appendChild(releaseCover);
        releaseLink.appendChild(releaseTitle);
        release.appendChild(releaseLink);
        musicGridDisplay.appendChild(release);

    }
}


headerLogoLink.addEventListener('click', function(){
    header.scrollIntoView();
});

musicDetailFooterNavBackLink.addEventListener('click', function(){
    header.scrollIntoView();
});

parsedReleases.reverse();
setReleaseDetail();
displayGrid(parsedReleases);